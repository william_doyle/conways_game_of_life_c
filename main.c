#include "conways_logic/life.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "show_field.h"
#include <ncurses.h>
#include "cpy_cells.h"
#include "sim_once.h"
#include <unistd.h>
#include <locale.h>

#define DEBUG
#include "stubber.h"

//#define FANCY_GRAPHICSi //if defined use opengl graphics

#define BLACK_SQUARE "\u25A0"
#define WHITE_SQUARE "\u25A1"

#define NGENS 10000									/* number of generations to run */
#define DEFAULT_SPEED 100000

int main (int argc, char ** argv){
	struct cell * cells;								/* holds cells --like 2d array navigated with ptr arithmatic */
	struct cell * old_state;							/* holds a lagged copy of the cells */
	unsigned nrows;									/* number rows */
	unsigned ncols;									/* number coloums */
	char * seedfname;								/* name of file containing seed */
	unsigned speed;
	speed = DEFAULT_SPEED;
	
	if (argc < 4){
		printf("Warning! I need at least 3 arguments!\n");
		goto END; /* clean simple use of goto */
	}else {										/* zeroth arg is always name of executable */
		nrows = atoi(argv[1]);							/* first arg is number of rows */
		ncols = atoi(argv[2]);							/* second arg is number of cols */
		seedfname = argv[3];							/* third arg is seed file name */
		if (argc >= 5){
			speed = atoi(argv[4]);
		}
	}

	/* get rows, coloums, and seed file from arguments */
	cells = malloc(nrows*ncols*sizeof(struct cell));					/* grow cells --container of cell(s) */ 
	old_state = malloc(nrows*ncols*sizeof(struct cell));		
	
	/* populate array with values from file --pointer arithmatic navigation */
	FILE * fp = fopen(seedfname, "r");						/* open file for reading */
	for (int rowi = 0; rowi < nrows; rowi++){					/* rowi -> row index */
		for (int coli = 0; coli < ncols; coli++){				/* coli -> col index */
			int tmp;
			fscanf(fp, "%d", &tmp);						/* read next int from file */
			init_cell((cells + rowi*ncols+coli) , tmp, rowi, coli); 	/* construct cell at rowi coli with (bool)int from file */
		}// close for coli
	}//close for rowi
	fclose(fp);									/* close seed file */
	

	/*    n c u r s e s    s t u f f    */
	setlocale(P_ALL, "");
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);

	show_field(&cells, nrows, ncols, BLACK_SQUARE, WHITE_SQUARE);			/* draw field in terminal */

	for (int i = 0; i < NGENS; i++){						/* run some generations */
		wmove(stdscr, 0, 0);
		refresh();
		cpy_cells(&cells, &old_state, nrows, ncols);				/* copy cells into old_state --update old_state */
		sim_one_gen(cells, old_state, nrows, ncols);				/* simulate one generation */
		//puts("");								/* put a gap before the next field is drawn */
		//show_field(&cells, nrows, ncols, "A", "D");		/* draw field in terminal */
		show_field(&cells, nrows, ncols, BLACK_SQUARE, WHITE_SQUARE);		/* draw field in terminal */
		usleep(speed);
	}
	
	/* free all allocated memory */
NOT_QUITE_THE_END:	
	endwin();
	free(cells);
END:
	return EXIT_SUCCESS;
};


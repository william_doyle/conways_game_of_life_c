#pragma once
//#include <ncurses.h>
#include <stdio.h>
#include <ncurses.h>

static inline void show_field(struct cell ** cells, unsigned nrows, unsigned ncols, char* calive, char* cdead){
	for (int rowi = 0; rowi < nrows; rowi++){			
		for (int coli = 0; coli < ncols; coli++){			
			printw("%*s",4, ((*cells + rowi*ncols+coli)->state)? calive:cdead );
		}
		printw("\n");
	}
};

#pragma once
#include "conways_logic/life.h"

static inline void cpy_cells(struct cell ** cells, struct cell ** old_state, unsigned nrows, unsigned ncols){
	for (int rowi = 0; rowi < nrows; rowi++){
		for (int coli = 0; coli < ncols; coli++){
			*(*old_state + rowi*ncols+coli) = *(*cells + rowi*ncols+coli);
		}
	}
};

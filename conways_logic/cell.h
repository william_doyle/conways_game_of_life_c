#pragma once
/*
 	A cell in Conways game of life
	William Doyle 
	January 18th 2019
*/

struct cell{
	bool state; /* alive is true , dead is false */
	void (*show)(void);
	void (*state_update)(struct cell *, struct cell *, unsigned, unsigned);
	unsigned row;
	unsigned col;
};




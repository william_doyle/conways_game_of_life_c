#include "life.h"


void init_cell(struct cell * self, bool aliveness, unsigned _row, unsigned _col){
	self->state = aliveness;		//set the state of cell 
	self->state_update = update_state;	//set the state_update function
	self->row = _row;			//set the row value
	self->col = _col;			//set the col value
};

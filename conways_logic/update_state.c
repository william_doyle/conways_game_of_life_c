#include "life.h"

void update_state(struct cell * self, struct cell * all, unsigned nrows, unsigned ncols){
	bool locals [8];			//only intrested in these cells
	unsigned lo_index = 0;			//what local shall we write to next?
	unsigned lo_alive = 0;			//count of all the locals with state value of true

//	( all + ((ncols*self->row) + self->col) )
	for (int i = 0; i < nrows; i++){
		for (int k = 0; k < ncols; k++){
			if (((all+i*ncols+k)->col+1 == self->col)||((all+i*ncols+k)->col-1 == self->col)||((all+i*ncols+k)->col == self->col) ){
				if (((all+i*ncols+k)->row+1 == self->row)||((all+i*ncols+k)->row-1 == self->row)||((all+i*ncols+k)->row == self->row) ){
					if (((all+i*ncols+k)->col == self->col)&&((all+i*ncols+k)->row == self->row)){
						continue;
					}
					locals[lo_index] = (all+i*ncols+k)->state;
					lo_index++;
					if (lo_index >= 8){
						goto END_BUILD_LOCALS;
					}//close 3rd layer if
				}//close 2nd layer if
			}//close 1st layer if
		}//close for k 
	}//close for i

END_BUILD_LOCALS:	/* AT THIS POINT WE HAVE ALL EIGHT NEIGHBOURS ACCOUNTED FOR */
	/* count alive */
	for (int i = 0; i < 8; i++){
	       if (locals[i]){
		       lo_alive++;
	       }
	}

	/* 1. Any live cell with fewer than two live neighbours dies, as if by underpopulation  */
	/* 2. Any live cell with two or three live neighbours lives on to the next generation  */
	/* 3. Any live cell with more than three live neighbours dies, as if by over population */
	/* 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction */

	/* 1  2  3 */
	if (self->state == true){
		/* 1 */
		if (lo_alive < 2){
			self->state = false; //die
		}
		/* 2 */
		if(lo_alive == 2 || lo_alive == 3){
			/* live on */
		}
		/* 3 */
		if(lo_alive > 3){
			self->state = false; //die
		}
	}//close outer if
	/* 4 */
	else {
		if (lo_alive == 3){
			self->state = true;  //come to life
		}
	}//close else	
};


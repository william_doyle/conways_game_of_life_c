#pragma once
#include <stdbool.h>
#include "cell.h"

#define ROW_COUNT 15
#define COL_COUNT 15

void init_cell(struct cell *, bool, unsigned, unsigned);
void update_state(struct cell * , struct cell * , unsigned, unsigned);


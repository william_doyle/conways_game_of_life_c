Dependencies:

Name		How to get
ncurses		sudo apt-get install libncurses5-dev libncursesw5-dev

Compile:
gcc main.c conways_logic/init_cell.c conways_logic/update_state.c -lncursesw

Run: 
	./a.out [number rows] [number cols] [seed file] [!optional: speed]

Intresting examples:
	./a.out 43 43 seeds/seed4.txt 

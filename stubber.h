#pragma once

/*
 	A good system for stubbing
	William Doyle
	January 19th 2020

	Note: To use this stubbing tool define 'DEBUG' before including this
	file. Like this:
		#define DEBUG
		#include "stubber.h"
 */
int next_stub = 0;
static inline void _stub(const char * funk){
#ifdef DEBUG
	extern int next_stub;
	printf("Stub %d in %s\n", next_stub++, funk);
#endif
};
#define STUB _stub(__func__);

#pragma once

static inline void sim_one_gen(struct cell * cells, struct cell * old_state, unsigned nrows, unsigned ncols){
	for (int rowi = 0; rowi < nrows; rowi++){
		for (int coli = 0; coli < ncols; coli++){	
			(cells+rowi*ncols+coli)->state_update( (cells+rowi*ncols+coli), old_state, nrows, ncols);
		}
	}
};

